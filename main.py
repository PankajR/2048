# -*- coding: utf-8 -*-
"""
Created on Wed Jan  1 09:25:36 2020

@author: PR
"""
#import libraries
import random,copy
from colorama import Fore, Back, Style

#global list(matrix)
arr = []
#game over no further moves possible
gameOver = False
#game status
game_status = None

#initialize matrix to 0
def init():
    global arr
    arr = [[0 for k in range(4)] for _ in range(4)]
               
#move matrix along the row (replace non zero with zero)
def move_horizontal(direction="r"):
    global arr
    for i in range(len(arr)):
        j_range = range(len(arr[i])) if direction == 'l' else range(len(arr[i])-1,-1,-1)
        for j in j_range:
            k_range = range(j+1,len(arr[i])) if direction == 'l' else range(j-1,-1,-1)
            for k in k_range:
                if arr[i][j] == 0 and arr[i][k] != 0:
                    arr[i][j],arr[i][k] = arr[i][k],0
       
#merge same elements in row
def perform_horizontal(direction="r"):
    global arr
    move_horizontal(direction=direction)
    #calculate
    j_mod = 1 if direction == 'l' else -1
    for i in range(len(arr)):
        j_range = range(len(arr[i])-1) if direction == 'l' else range (len(arr[i])-1,0,-1)
        for j in j_range:
            if arr[i][j] == arr[i][j+j_mod]:
                arr[i][j],arr[i][j+j_mod] = arr[i][j]*2,0
    move_horizontal(direction=direction)
                
#move matrix along column (replace non zero with zero)
def move_vertical(direction='u'):
    global arr
    
    for i in range(len(arr)):
        j_range = range(len(arr)) if direction == 'u' else range(len(arr[i])-1,-1,-1)
        for j in j_range:
            k_range = range(j+1,len(arr[i])) if direction == 'u' else range(j-1,-1,-1)
            for k in k_range:
                if arr[j][i] == 0 and arr[k][i] != 0:
                    arr[j][i],arr[k][i] = arr[k][i],0

#merge same elements along column
def perform_vertical(direction="u"):
    global arr
    move_vertical(direction=direction)
    
    j_mod = 1 if direction == 'u' else -1
    for i in range(len(arr)):
        j_range = range(len(arr)-1) if direction == 'u' else range (len(arr)-1,0,-1)
        for j in j_range:
            if arr[j][i] == arr[j+j_mod][i]:
                arr[j][i],arr[j+j_mod][i] = arr[j][i]*2,0
                
    move_vertical(direction=direction)

#Draw matrix
def draw():
    global arr, game_status
    import os
    os.system("cls")
    print("-"*((len(arr)*8)+1))
    for i in arr:
        print("|",end="")
        for j in i:
            print("{:^7}|".format(""), end="")
        print()
        print("|",end="")
        for j in i:
            s = j if j != 0 else ""
            if str(s) == "2048": game_status = "You won! Now endless mode starts"
            print(Fore.GREEN+"{:^7}".format(s)+Style.RESET_ALL+"|", end="")
        print()
        print("|",end="")
        for j in i:
            print("{:^7}|".format(""), end="")
        print()
        print("-"*((len(arr)*8)+1))
    print("left: a, up: w, right: d, down: s")
    if game_status: print(game_status)
    print(Style.RESET_ALL,end="")
    
#Generate 2 at random place where value is zero 
def generate_random():
    global arr, gameOver
    ls = []
    for i in range(len(arr)):
        for j in range(len(arr)):
            if arr[i][j] == 0:
                ls.append((i,j))
    if ls == []: #no cell with value 0 so not further moves possible
        gameOver = True
    else:
        c = random.choice(ls)
        arr[c[0]][c[1]] = 2
    
if __name__ == '__main__':
    init()
    import msvcrt
    generate_random()
    while not gameOver:
        temp = copy.deepcopy(arr)
        draw()
        ch = msvcrt.getwch() #left: a, up: w, right: d, down: s
        if ch == 'a':
            perform_horizontal(direction='l')
        elif ch == 'd':
            perform_horizontal(direction='r')
        elif ch == 'w':
            perform_vertical(direction='u')
        elif ch == 's':
            perform_vertical(direction='d')
        elif ch == 'q':
            exit()
        else:
            pass
        if temp != arr: #prev step matrix and current matrix should not same
            generate_random()
        else:
            print(Fore.RED+"Invalid move"+Style.RESET_ALL)
    if game_status is None:print("Oops Game Over. No further moves are possible") 
    if game_status is not None:print("You won the game but no further moves are possible")